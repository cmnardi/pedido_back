<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use \dogs\Model\Cliente as Cliente;

class ClienteTest extends TestCase
{

    private $cliente = null;
    use WithoutMiddleware;
    //use DatabaseTransactions;
    //use DatabaseMigrations;

    public function testInsert()
    {
        echo "\nCLIENTE\t";
        $this->cliente = factory(Cliente::class)->make();
        $response = $this->call('POST', '/cliente',
            ['nome' => $this->cliente->nome
                , 'email'=>$this->cliente->email
                ,'telefone'=>$this->cliente->telefone
                ,'senha'=>'123456'
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->created);
        $this->assertNotNull($result->cliente->id);
//        echo "\nINS #".$result->cliente->id." \t ".$result->cliente->nome;
        return $result->cliente->id;
    }

    public function testInsertRemover()
    {
        echo "\nCLIENTE 2\t";
        $this->cliente = factory(Cliente::class)->make();
        $response = $this->call('POST', '/cliente',
            ['nome' => $this->cliente->nome
            , 'email'=>$this->cliente->email
            ,'telefone'=>$this->cliente->telefone
            ,'senha'=>'123456'
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->created);
        $this->assertNotNull($result->cliente->id);
//        echo "\nINS #".$result->cliente->id." \t ".$result->cliente->nome;
        return $result->cliente->id;
    }

    /**
     * Testa a listagem de cliente
     *
     * @return void
     */
    public function testList()
    {
        $this->get('/cliente', [])
            ->seeJsonStructure([[
                'nome',
                'email',
                'telefone'
            ]]);
    }

    /**
     * @depends testInsert
     */
    public function testUpdate($id)
    {
        $response = $this->call('POST', '/cliente',
            ['id' => $id,
                'nome' => 'nome upd']);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->updated);
        $this->assertNotNull($result->cliente->id);
        $this->assertEquals('nome upd',$result->cliente->nome);
//        echo "\nUPD #".$id;
    }

    /**
     * @depends testInsert
     * @param $id
     */
    public function testGet($id)
    {
//        echo "\nGET #".$id;
        $this->get('/cliente/'.$id)
            ->seeJsonStructure([
                'nome',
                'email',
                'telefone'
            ]);
    }

    /**
     * @depends testInsert
     * @param $id
     */
    public function testLogin($id)
    {
        $response = $this->call('GET', '/cliente/'.$id);
        $obj = json_decode($response->getContent());
        $response_login = $this->call('POST', '/login', [
            'email'=>$obj->email
            ,'senha'=>'123456'
        ]);
        $result = json_decode($response_login->getContent());
        $this->assertTrue($result->result);
        $this->assertNotNull($result->user->api_token);
        return $result->user->api_token;
    }

    /**
     * @depends testInsertRemover
     * @param $id
     */
    public function testLoginRemover($id)
    {
        $response = $this->call('GET', '/cliente/'.$id);
        $obj = json_decode($response->getContent());
        $response_login = $this->call('POST', '/login', [
            'email'=>$obj->email
            ,'senha'=>'123456'
        ]);
        $result = json_decode($response_login->getContent());
        $this->assertTrue($result->result);
        $this->assertNotNull($result->user->api_token);
        return $result->user->api_token;
    }


    /**
     * @depends testLoginRemover
     * @param $token
     */
    public function testLogff($token)
    {
        $response_login = $this->call('GET', '/logoff', [
            'api_token'=> $token
        ]);
        $result = json_decode($response_login->getContent());
        $this->assertTrue($result->result);

    }

    /**
     * @depends testInsertRemover
     */
    public function testDelete($id)
    {
        $this->delete('/cliente/'.$id)
            ->seeJson(['deleted' => true,
            ]);
    }

}
