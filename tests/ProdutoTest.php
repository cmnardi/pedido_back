<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use dogs\Model\Produto;

class ProdutoTest extends TestCase
{

    use WithoutMiddleware;
    
    public function testInsert()
    {
        echo "\nPRODUTO\t";
        $produto = factory(Produto::class)->make();
        $response = $this->call('POST', '/produto',
            ['nome' => $produto->nome
                , 'doce'=>$produto->doce
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->created);
        $this->assertNotNull($result->produto->id);
        return $result->produto->id;
    }

    /**
     * @depends testInsert
     * @param $id
     */
    public function testGet($id)
    {
        $this->get('/produto/'.$id)
            ->seeJsonStructure([
                'nome',
                'doce'
            ]);
    }

    /**
     * @depends testInsert
     */
    public function testList()
    {
        $this->get('/produto', [])
            ->seeJsonStructure([[
                'nome',
                'doce'
            ]]);
    }

    /**
     * @depends testInsert
     * @param $id
     */
    public function testUpdate($id)
    {
        $response = $this->call('POST', '/produto',
            ['id' => $id,
                'nome' => 'NOME UPD',
                'doce' => true
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->updated);
        $this->assertNotNull($result->produto->id);
        $this->assertEquals('NOME UPD',$result->produto->nome);
    }

    /**
     *
     */
    public function testUpdateNotFound()
    {
        $response = $this->call('POST', '/produto',
            ['id' => 99999,
                'nome' => 'NOME 404',
                'doce' => true
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(false, $result->updated);
        $this->assertNull($result->produto);
    }

    /**
     * @depends testInsert
     * @param $id
     */
    public function testDelete($id)
    {
        $this->delete('/produto/'.$id)
            ->seeJson(['deleted' => true,
            ]);
    }
}
