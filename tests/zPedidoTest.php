<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use dogs\Model\Pedido;

class zPedidoTest extends TestCase
{
    //use WithoutMiddleware;
    
    /**
     * @depends ClienteTest::testLogin
     *
     * @return mixed
     */
    public function testInsert($api_token)
    {
        echo "\nPEDIDO\t".$api_token;
        $pedido = factory(Pedido::class)->make();
        $response = $this->call('POST', '/pedido',
            ['api_token' => $api_token
                , 'entrega'=>$pedido->entrega
                ,'observacoes'=>$pedido->observacoes]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->created);
        $this->assertNotNull($result->pedido->id);
        return $result->pedido->id;
    }

//    /**
//     * @depends ClienteTest::testLogin
//     *
//     * Tenta inserir sem um cliente
//     */
//    public function testInsertFail($api_token)
//    {
//        $pedido = factory(Pedido::class)->make();
//        $response = $this->call('POST', '/pedido',
//            ['api_token' => $api_token
//                ]);
//        $result = json_decode($response->getContent());
//        $this->assertEquals(false, $result->created);
//        $this->assertNull($result->pedido);
//    }

    /**
     * Adicionar um item ao pedido
     *
     * @depends testInsert
     * @depends ProdutoTest::testInsert
     * @depends ClienteTest::testLogin
     *
     * @param $id_pedido
     * @param $id_produto
     */
    public function testSaveWithItens($id_pedido, $id_produto, $api_token) {
        $response = $this->call('POST', '/pedido',
            ['id' => $id_pedido,
                'api_token' => $api_token,
                'itens'=> [
                    ['quantidade'=>2,'id_produto'=>$id_produto]
                    ,['quantidade'=>4,'id_produto'=>$id_produto]
                ]]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->updated);
        $this->assertNotNull($result->pedido->id);
        return $result->pedido->id;
    }

    /**
     * Adicionar um item ao pedido
     *
     * @depends ClienteTest::testLogin
     * @depends ProdutoTest::testInsert
     *
     * @param $id_produto
     */
    public function testInsertWithItens($api_token, $id_produto) {
        $pedido = factory(Pedido::class)->make();
        $response = $this->call('POST', '/pedido',
            ['id' => null
                ,'api_token' => $api_token
                ,'entrega'=>$pedido->entrega
                ,'observacoes'=>$pedido->observacoes
                ,'itens'=> [
                    ['quantidade'=>8,'id_produto'=>$id_produto]
                    ,['quantidade'=>12,'id_produto'=>$id_produto]
                ]]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->created);
        $this->assertNotNull($result->pedido->id);
        return $result->pedido->id;
    }

    /**
     * @depends testInsert
     * @depends ClienteTest::testLogin
     *
     * @param $id
     */
    public function testGet($id, $api_token)
    {
        $this->get('/pedido/'.$id.'?api_token='.$api_token)
            ->seeJsonStructure([
                'id_cliente',
                'entrega',
                'observacoes'
            ]);
    }

    /**
     * @depends ClienteTest::testLogin
     *
     */
    public function testList($api_token)
    {
        $this->get('/pedido?api_token='.$api_token, [])
            ->seeJsonStructure([[
                'id_cliente',
                'entrega',
                'observacoes'
            ]]);
    }

    /**
     * @depends testInsert
     * @depends ClienteTest::testLogin
     *
     * @param $id
     */
    public function testUpdate($id, $api_token)
    {
        $response = $this->call('POST', '/pedido',
            ['id' => $id,
                'id_cliente' => 1,
                'api_token' => $api_token,
                'entrega' => '2016-12-31',
                'observacoes' => 'Alterada obs'
            ]);
        $result = json_decode($response->getContent());
        $this->assertEquals(true, $result->updated);
        $this->assertNotNull($result->pedido->id);
        $this->assertEquals('Alterada obs',$result->pedido->observacoes);
    }

    /**
     * @depends testInsert
     * @depends ClienteTest::testLogin
     *
     * @param $id
     */
    public function testDelete($id, $api_token)
    {
        $this->delete('/pedido/'.$id.'?api_token='.$api_token)
            ->seeJson(['deleted' => true,
            ]);
    }


}
