<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pedido', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente')->unsigned();
            $table->date('entrega');
            $table->string('observacoes');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_cliente')->references('id')->on('cliente');
        });

        Schema::create('pedido_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_produto')->unsigned();
            $table->integer('quantidade');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_produto')->references('id')->on('produto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pedido_item');
        Schema::drop('pedido');
    }
}
