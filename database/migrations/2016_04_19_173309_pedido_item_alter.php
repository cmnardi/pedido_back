<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PedidoItemAlter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pedido_item', function ($table) {
            $table->integer('id_pedido')->unsigned();

            $table->foreign('id_pedido')->references('id')->on('pedido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pedido_item', function ($table) {
            $table->dropForeign('pedido_item_id_pedido_foreign');
            $table->dropColumn(['id_pedido']);
        });
    }
}
