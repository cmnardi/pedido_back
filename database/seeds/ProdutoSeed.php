<?php

use Illuminate\Database\Seeder;

class ProdutoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('produto')->insert(
            array(
                array(
                    'nome' => 'Brigadeiro',
                    'doce' => true
                ),
               array(
                    'nome' => 'Beijinho',
                    'doce' => true
                ),
                 array(
                    'nome' => 'Coxinha',
                    'doce' => false
                ),
                
            ));
    }
}
