<?php

use Illuminate\Database\Seeder;

class Cliente extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cliente')->insert(
            array(
                array(
                    'nome' => 'Teste',
                    'email' => 'teste2@teste.com',
                    'senha' => Hash::make('123'),
                    'api_token' => str_random(60)
                )
            ));
    }


}
