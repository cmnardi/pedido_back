<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(dogs\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(dogs\Model\Cliente::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->name,
        'email' => $faker->safeEmail,
        'telefone' => $faker->phoneNumber
    ];
});


$factory->define(dogs\Model\Pedido::class, function (Faker\Generator $faker) {
    return [
        'id_cliente' => 1,
        'entrega' => $faker->date(),
        'observacoes' => $faker->text()
    ];
});

$factory->define(dogs\Model\Produto::class, function (Faker\Generator $faker) {
    return [
        'nome' => $faker->shuffleString(),
        'doce' => $faker->boolean()
    ];
});