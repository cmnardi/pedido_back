<?php

namespace dogs\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(
            \dogs\Repositories\Contracts\ClienteRepositoryInterface::class,
            \dogs\Repositories\ClienteRepository::class
        );
        $this->app->bind(
            \dogs\Repositories\Contracts\ProdutoRepositoryInterface::class
            ,\dogs\Repositories\ProdutoRepository::class
        );
        $this->app->bind(
            \dogs\Repositories\Contracts\PedidoRepositoryInterface::class
            ,\dogs\Repositories\PedidoRepository::class
        );
        $this->app->bind(
            \dogs\Repositories\Contracts\PedidoItemRepositoryInterface::class
            ,\dogs\Repositories\PedidoItemRepository::class
        );
    }
}
