<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Cliente@index');
Route::post('/login', 'ClienteController@login');
Route::get('/logoff', 'ClienteController@logoff');

Route::post('cliente/', 'ClienteController@save');

Route::group(['prefix' => 'cliente', 'middleware'=>'auth:api'], function () {
    Route::get('/', 'ClienteController@index');
    Route::get('/{id}', 'ClienteController@getCliente');
    Route::delete('/{id}', 'ClienteController@rmCliente');
});

Route::group(['prefix' => 'pedido', 'middleware'=>'auth:api'], function () {
    Route::get('/', 'PedidoController@index');
    Route::get('/{id}', 'PedidoController@getPedido');
    Route::post('', 'PedidoController@save');
    Route::delete('/{id}', 'PedidoController@rmPedido');
});


Route::group(['middleware' => 'cors'], function () {
    Route::get('produto/', 'ProdutoController@index');
    Route::get('produto/{id}', 'ProdutoController@getProduto');
});

Route::group(['prefix' => 'produto', 'middleware'=>'auth:api'], function () {
    Route::post('', 'ProdutoController@save');
    Route::delete('/{id}', 'ProdutoController@rmProduto');
});