<?php

namespace dogs\Http\Controllers;

use dogs\Http\Requests;
use Illuminate\Http\Request;
use dogs\Repositories\Contracts\ProdutoRepositoryInterface;


class ProdutoController extends Controller
{
    //
    public function index(ProdutoRepositoryInterface $repository)
    {
        return $repository->listAll();
    }

    public function save(Request $request, ProdutoRepositoryInterface $repository)
    {
        $id = $request->id;
        $response = (is_null($id)) ? 'created' : 'updated';
        $produto = [];
        try {
            if (!is_null($request->id)) {
                $produtoObj = $repository->find($request->id);
                if ( is_null( $produtoObj ) ) {
                    throw new \Exception('Produto não encontrado!');
                }
            }
            if (!is_null($request->nome)) $produto['nome'] = $request->nome;
            if (!is_null($request->doce)) $produto['doce'] = $request->doce;

            $result = $repository->save($produto);
            return [$response => $result
                , 'produto' => $repository->getModel()
            ];
        }catch (\Exception $e ){
            return [$response => false
                , 'produto' => null
                , 'error' => $e->getMessage()
            ];
        }
    }

    public function getProduto($id, ProdutoRepositoryInterface $repository)
    {
        return $repository->find($id);
    }

    public function rmProduto($id, ProdutoRepositoryInterface $repository)
    {
        $result = $repository->delete($id);
        return ['deleted' => $result];
    }
}
