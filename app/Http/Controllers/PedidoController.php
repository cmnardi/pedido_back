<?php

namespace dogs\Http\Controllers;

use Illuminate\Http\Request;
use \dogs\Repositories\Contracts\PedidoRepositoryInterface;
use \dogs\Repositories\Contracts\PedidoItemRepositoryInterface;

use dogs\Http\Requests;

class PedidoController extends Controller
{
    //
    public function index(PedidoRepositoryInterface $repository)
    {
        $user = \Request::get('user');
        return $repository->listByIdCliente($user->id);
    }

    public function save(Request $request, PedidoRepositoryInterface $repository)
    {
        $id = $request->id;
        $response = (is_null($id)) ? 'created' : 'updated';
        $pedido = [];
        try {
            $user = \Request::get('user');
            if (!is_null($request->id)) {
                $pedidoObj = $repository->find($request->id);
                if ( is_null( $pedidoObj ) ) {
                    throw new \Exception('Pedido não encontrado!');
                }
                if ( $pedidoObj->id_cliente != $user->id ){
                    throw new \Exception('Pedido não pertence ao cliente!');
                }
            }
            //if (!is_null($request->id_cliente)) $pedido['id_cliente'] = $request->id_cliente;
            $pedido['id_cliente'] = $user->id;
            if (!is_null($request->entrega)) $pedido['entrega'] = $request->entrega;
            if (!is_null($request->observacoes)) $pedido['observacoes'] = $request->observacoes;

            $result = $repository->save($pedido);
            if ( !is_null ($request->itens) && is_array($request->itens) && count($request->itens)){
                $itens = array();
                foreach($request->itens as $item_vars ) {
                    $itens[] =[
                        'id_pedido'=>$repository->getModel()->id,
                        'quantidade'=>$item_vars['quantidade']
                        ,'id_produto'=>$item_vars['id_produto']
                    ];
                }
                $repository->saveItens($itens);
            }
            return [$response => $result
                , 'pedido' => $repository->getModel()
            ];
        }catch (\Exception $e ){
            return [$response => false
                , 'pedido' => null
                , 'error' => $e->getMessage()
            ];
        }
    }

    public function getPedido($id, PedidoRepositoryInterface $repository)
    {
        return $repository->find($id);
    }

    public function rmPedido($id, PedidoRepositoryInterface $repository)
    {
        $pedido = $repository->find($id);
        $result = false;
        if(!is_null($pedido)) {
            $result = $pedido->delete();
        }
        return ['deleted' => $result];
    }
}
