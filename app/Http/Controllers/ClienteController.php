<?php

namespace dogs\Http\Controllers;

use dogs\Http\Requests;
use dogs\Repositories\Contracts\ClienteRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

//use dogs\Model\Cliente as Cliente;

class ClienteController extends Controller
{
    //
    public function index(ClienteRepositoryInterface $repository)
    {
        return $repository->listAll();
    }

    public function save(Request $request, ClienteRepositoryInterface $repository)
    {
        $id = $request->id;
        $response = (is_null($id)) ? 'created' : 'updated';
        $cliente = [];
        if (!is_null($request->id)) {
            $repository->find($request->id);
        }
        else {
            $cliente['api_token'] = str_random(10);
        }
        if (!is_null($request->nome)) $cliente['nome'] = $request->nome;
        if (!is_null($request->email)) $cliente['email'] = $request->email;
        if (!is_null($request->telefone)) $cliente['telefone'] = $request->telefone;
        if (!is_null($request->senha)) $cliente['senha'] = \Hash::make($request->senha);

        try {
            $result = $repository->save($cliente);
        } catch (\Exception $e) {
            $result = false;
            return [$response => false
                , 'error' => $e->getMessage()
            ];
        }
        return [$response => $result
            , 'cliente' => $repository->getModel()
        ];
    }

    public function rmCliente($id, ClienteRepositoryInterface $repository)
    {
        $result = false;
        $result = $repository->delete($id);
        return ['deleted' => $result];
    }

    public function getCliente($id, ClienteRepositoryInterface $repository)
    {
        return $repository->find($id);
    }

    public function login()
    {
// validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'senha' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return ['result'=>false,
            'message' => 'Input incorreto'];
        }
        // create our user data for the authentication
        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('senha'),
        );
        // attempt to do the login
        if (\Auth::attempt($userdata)) {
            $token =  str_random(60);
            $user = \Auth::user();
            $user->api_token = $token;
            $user->save();
            return
                ['result'=>true ,
                    'user'=>$user];
        }
        // validation not successful
        return [
            'result'=>false
            ,'message' => 'Login falhou!'
        ];
    }

    public function logoff()
    {
        $result = false;
        $user = \Auth::guard('api')->user();
        if ( !is_null($user)) {
            \Auth::logout();
            $user->api_token = str_random(10);
            $result = $user->save();
        }
        return [
            'result'=>$result
        ];
    }
}
