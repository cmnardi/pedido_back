<?php

namespace dogs\Repositories;

use dogs\Repositories\Contracts\ClienteRepositoryInterface;
use dogs\Repositories\AbstractRepository;
use dogs\Model\Cliente;

class ClienteRepository extends AbstractRepository implements ClienteRepositoryInterface
{
    /**
     * @var Cliente
     */
    protected $model;

    public function __construct(Cliente $model)
    {
        $this->model = $model;
    }
}