<?php
namespace dogs\Repositories\Contracts;


interface PedidoRepositoryInterface extends RepositoryInterface {

    public function saveItens($itens);

    public function listByIdCliente($id_cliente);
}