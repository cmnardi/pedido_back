<?php
namespace dogs\Repositories\Contracts;


interface RepositoryInterface
{
    public function listAll();

    public function save(array $data);

    public function delete($id);

    public function find($id);
}