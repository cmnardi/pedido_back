<?php

namespace dogs\Repositories;

use dogs\Repositories\Contracts\ProdutoRepositoryInterface;
use dogs\Model\Produto;

class ProdutoRepository extends AbstractRepository implements ProdutoRepositoryInterface
{
    /**
     * @var Cliente
     */
    protected $model;

    public function __construct(Produto $model)
    {
        $this->model = $model;
    }
}