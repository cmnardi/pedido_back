<?php

namespace dogs\Repositories;


use dogs\Model\Pedido;
use dogs\Model\PedidoItem;
use dogs\Repositories\Contracts\PedidoRepositoryInterface;

class PedidoRepository extends AbstractRepository implements PedidoRepositoryInterface
{
    public function __construct(Pedido $model)
    {
        $this->model = $model;
    }

    public function saveItens($itens)
    {
        foreach($itens as $item) {
            $itemObj = new PedidoItem($item);
            $itemObj->save();
        }
    }
    
    public function listByIdCliente($id_cliente)
    {
        return $this->model->where('id_cliente', $id_cliente)->get();
    }
}