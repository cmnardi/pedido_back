<?php

namespace dogs\Repositories;


use dogs\Model\PedidoItem;
use dogs\Repositories\Contracts\PedidoRepositoryInterface;

class PedidoItemRepository extends AbstractRepository implements PedidoRepositoryInterface
{
    public function __construct(PedidoItem $model)
    {
        $this->model = $model;
    }
}