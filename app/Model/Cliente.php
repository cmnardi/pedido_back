<?php

namespace dogs\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableInterface;

class Cliente extends Authenticatable
{
    //
    use SoftDeletes;
    
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cliente';

    protected $fillable = ['nome','email','telefone', 'api_token', 'senha'];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function pedidos()
    {
        return $this->hasMany('dos\Model\Pedido', 'id_cliente','id');
    }


}
