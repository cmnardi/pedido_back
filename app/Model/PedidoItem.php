<?php

namespace dogs\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PedidoItem extends Model
{
    //
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pedido_item';

    protected $fillable = ['id_produto','id_pedido','quantidade'];

    public function pedido()
    {
        return $this->belongsTo('dogs\Model\Pedido');
    }

    public function produto()
    {
        return $this->belongsTo('dogs\Model\Produto');
    }
}
