<?php

namespace dogs\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pedido extends Model
{
    //
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pedido';

    protected $fillable = ['id_cliente','entrega','observacoes'];

    public function cliente()
    {
        return $this->belongsTo('dogs\Model\Cliente');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itens()
    {
        return $this->hasMany('dogs\Model\PedidoItem','id_pedido','id');
    }
}
